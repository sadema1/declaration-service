create database if not exists member;

use member;
-- create table if not exists hibernate_sequence
-- (
--     next_not_cached_value bigint(21)          not null,
--     minimum_value         bigint(21)          not null,
--     maximum_value         bigint(21)          not null,
--     start_value           bigint(21)          not null comment 'start value when sequences is created or value if RESTART is used',
--     increment             bigint(21)          not null comment 'increment value',
--     cache_size            bigint(21) unsigned not null,
--     cycle_option          tinyint(1) unsigned not null comment '0 if no cycles are allowed, 1 if the sequence should begin a new cycle when maximum_value is passed',
--     cycle_count           bigint(21)          not null comment 'How many cycles have been done'
--     );

drop sequence if exists hibernate_sequence;
drop table if exists topicpartition;
drop table if exists uuidbase_event_entity;
drop table if exists member_base_event_entity;
drop table if exists address_assigned_to_member_event;
drop table if exists address_unassigned_from_member_event;
drop table if exists member_kind_changed_event;
drop table if exists member_signed_up_event;

CREATE SEQUENCE hibernate_sequence INCREMENT 1 MINVALUE 1 MAXVALUE 922337203685477580 START 1 CACHE 1;

create table topicpartition
(
    id               bigint       not null primary key,
    partition_offset bigint       not null,
    partition_number int          not null,
    topic_name       varchar(255) not null
    );

create table if not exists uuidbase_event_entity
(
    id                 bigint       not null primary key,
    creation_date_time datetime(6)  not null,
    domain_event_name  varchar(255) not null,
    reference          varchar(255) not null
);

create table if not exists member_base_event_entity
(
    id                 bigint       not null primary key,
    creation_date_time datetime(6)  not null,
    domain_event_name  varchar(255) not null,
    reference          varchar(255) not null
);

create table if not exists address_assigned_to_member_event
(
    address_reference binary(255)  not null,
    city              varchar(255) null,
    street            varchar(255) null,
    street_number     varchar(255) not null,
    zip_code          varchar(255) not null,
    id                bigint       not null primary key,
    constraint foreign key (id) references member_base_event_entity (id)
    );

create table if not exists address_unassigned_from_member_event
(
    address_reference binary(255) not null,
    id                bigint      not null primary key,
    constraint foreign key (id) references member_base_event_entity (id)
    );

create table if not exists member_kind_changed_event
(
    kind varchar(255) null,
    id   bigint       not null primary key,
    constraint foreign key (id) references member_base_event_entity (id)
    );

create table if not exists member_signed_up_event
(
    birth_date date         null,
    first_name varchar(255) null,
    kind       varchar(255) null,
    last_name  varchar(255) null,
    id         bigint       not null primary key,
    constraint foreign key (id) references member_base_event_entity (id)
    );

