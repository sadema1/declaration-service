package nl.kristalsoftware.association.declarationservice.viewstore;

import nl.kristalsoftware.association.declarationservice.viewstore.document.DeclarationDocument;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;
import java.util.UUID;

public interface DeclarationDocumentsRepository extends MongoRepository<DeclarationDocument, ObjectId> {
    Optional<DeclarationDocument> findByReference(UUID value);

}

