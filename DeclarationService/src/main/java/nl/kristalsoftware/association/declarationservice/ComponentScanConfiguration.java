package nl.kristalsoftware.association.declarationservice;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = {
        "nl.kristalsoftware.ddd.eventstore.base",
        "nl.kristalsoftware.ddd.eventstream.base"
})
@Configuration
public class ComponentScanConfiguration {
}
