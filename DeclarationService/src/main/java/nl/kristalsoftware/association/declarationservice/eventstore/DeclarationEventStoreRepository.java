package nl.kristalsoftware.association.declarationservice.eventstore;


import nl.kristalsoftware.association.declarationservice.domain.declaration.aggregate.Declaration;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventLoading;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.UUID;

public interface DeclarationEventStoreRepository extends CrudRepository<DeclarationBaseEventEntity, Long> {

    Iterable<DeclarationBaseEventEntity<? extends DomainEventLoading<Declaration>>> findAllByReference(UUID value);

    Optional<DeclarationBaseEventEntity<?>> findFirstByReference(UUID value);

}
