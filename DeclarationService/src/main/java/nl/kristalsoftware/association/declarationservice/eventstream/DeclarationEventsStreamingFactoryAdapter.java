package nl.kristalsoftware.association.declarationservice.eventstream;

import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.declarationservice.domain.declaration.streams.DeclarationEventsStreamingFactoryPort;
import nl.kristalsoftware.association.declarationservice.domain.declaration.streams.DeclarationEventsStreamingPort;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class DeclarationEventsStreamingFactoryAdapter implements DeclarationEventsStreamingFactoryPort {

    private final DeclarationEventDataProducer declarationEventDataProducer;

    @Override
    public DeclarationEventsStreamingPort createStreamingAdapter() {
        return new DeclarationEventsStreamingAdapter(declarationEventDataProducer);
    }
}
