package nl.kristalsoftware.association.declarationservice.viewstore;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.declarationservice.domain.declaration.aggregate.Declaration;
import nl.kristalsoftware.association.declarationservice.domain.declaration.views.DeclarationDocumentsRepositoryPort;
import nl.kristalsoftware.association.declarationservice.viewstore.document.DeclarationDocument;

import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
public class DeclarationDocumentsRepositoryAdapter implements DeclarationDocumentsRepositoryPort {

    private final DeclarationDocumentsRepository declarationDocumentsRepository;
    private DeclarationDocument declarationDocument = new DeclarationDocument();

    @Override
    public void createDocument(Declaration aggregate) {
        Optional<DeclarationDocument> result = declarationDocumentsRepository.findByReference(aggregate.getReference().getValue());
        declarationDocument = result.orElseThrow(() -> new IllegalStateException(String.format("Document with reference %s not found in viewstore!", aggregate.getReference().getValue())));
    }

    @Override
    public void saveDocument() {
        declarationDocumentsRepository.save(declarationDocument);
    }

}
