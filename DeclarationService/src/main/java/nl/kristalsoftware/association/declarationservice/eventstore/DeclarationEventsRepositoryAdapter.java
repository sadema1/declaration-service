package nl.kristalsoftware.association.declarationservice.eventstore;

import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.declarationservice.domain.declaration.aggregate.Declaration;
import nl.kristalsoftware.association.declarationservice.domain.declaration.events.DeclarationEventsRepositoryPort;
import nl.kristalsoftware.ddd.domain.base.event.DomainEventLoading;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class DeclarationEventsRepositoryAdapter implements DeclarationEventsRepositoryPort {

    private final DeclarationEventStoreRepository declarationEventStoreRepository;

    @Override
    public List<DomainEventLoading<Declaration>> findAllDomainEventsByReference(Declaration aggregate) {
        List<DomainEventLoading<Declaration>> list = new ArrayList<>();
        for (DeclarationBaseEventEntity<? extends DomainEventLoading<Declaration>> it : declarationEventStoreRepository.findAllByReference(aggregate.getReference().getValue())) {
            DomainEventLoading<Declaration> domainEvent = it.getDomainEvent();
            list.add(domainEvent);
        }
        return list;
    }
}
