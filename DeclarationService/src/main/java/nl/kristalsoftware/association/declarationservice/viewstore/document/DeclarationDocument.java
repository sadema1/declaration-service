package nl.kristalsoftware.association.declarationservice.viewstore.document;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nl.kristalsoftware.association.declarationservice.domain.declaration.attributes.DeclarationReference;
import nl.kristalsoftware.association.declarationservice.domain.declaration.views.DeclarationView;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Data
@NoArgsConstructor
@Document
public class DeclarationDocument {

    @Setter(AccessLevel.NONE)
    @Id
    private ObjectId _id;
    private UUID reference;
    @Version
    private Long version;

    private DeclarationDocument(UUID reference) {
        this.reference = reference;
    }

    public static DeclarationDocument of(UUID reference) {
        return new DeclarationDocument(reference);
    }

    public DeclarationView toDeclarationView() {
        return DeclarationView.of(
                DeclarationReference.of(reference)
        );
    }
}
