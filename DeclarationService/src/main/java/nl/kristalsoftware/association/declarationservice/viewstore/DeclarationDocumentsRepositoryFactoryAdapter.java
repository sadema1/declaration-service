package nl.kristalsoftware.association.declarationservice.viewstore;

import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.declarationservice.domain.declaration.views.DeclarationDocumentsRepositoryFactoryPort;
import nl.kristalsoftware.association.declarationservice.domain.declaration.views.DeclarationDocumentsRepositoryPort;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class DeclarationDocumentsRepositoryFactoryAdapter implements DeclarationDocumentsRepositoryFactoryPort {

    private final DeclarationDocumentsRepository declarationDocumentsRepository;

    @Override
    public DeclarationDocumentsRepositoryPort createRepositoryAdapter() {
        return new DeclarationDocumentsRepositoryAdapter(declarationDocumentsRepository);
    }
}
