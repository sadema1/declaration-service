package nl.kristalsoftware.association.declarationservice.viewstore;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.declarationservice.domain.declaration.attributes.DeclarationReference;
import nl.kristalsoftware.association.declarationservice.domain.declaration.views.DeclarationView;
import nl.kristalsoftware.association.declarationservice.domain.declaration.views.DeclarationViewStorePort;
import nl.kristalsoftware.ddd.domain.base.aggregate.AggregateNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Component
public class DeclarationViewStoreAdapter implements DeclarationViewStorePort {

    private final DeclarationDocumentsRepository declarationDocumentsRepository;

    @Override
    public List<DeclarationView> getAllDeclarations() {
        return declarationDocumentsRepository.findAll().stream()
                .map(it -> it.toDeclarationView())
                .toList();
    }

    @Override
    public DeclarationView getDeclarationByReference(DeclarationReference declarationReference) throws AggregateNotFoundException {
        return declarationDocumentsRepository.findByReference(declarationReference.getValue())
                .map(it -> it.toDeclarationView()
                )
                .orElseThrow(() -> new AggregateNotFoundException(String.format("Declaration with reference: %s not found!", declarationReference.getValue())));
    }

}
