package nl.kristalsoftware.association.declarationservice.rest.command;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.declarationservice.domain.declaration.commands.DeclarationCommandService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/declarations", produces = MediaType.APPLICATION_JSON_VALUE)
@Tag(name = "Declaration commands", description = "Endpoints for updating declarations")
public class DeclarationCommandController {

    private final DeclarationCommandService declarationCommandService;

}
