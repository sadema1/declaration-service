package nl.kristalsoftware.association.declarationservice;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = {
        "nl.kristalsoftware.ddd.eventstore.base",
        "nl.kristalsoftware.ddd.eventstream.base",
        "nl.kristalsoftware.association.declarationservice.eventstore"
})
@EntityScan(basePackages = {
        "nl.kristalsoftware.ddd.eventstore.base",
        "nl.kristalsoftware.ddd.eventstream.base",
        "nl.kristalsoftware.association.declarationservice.eventstore"
})
@Configuration
public class JPAConfiguration {
}
