package nl.kristalsoftware.association.declarationservice.eventstream;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.declarationservice.eventstream.declaration.DeclarationEventData;
import nl.kristalsoftware.ddd.eventstream.base.producer.ByteArrayEventProducer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class DeclarationEventDataProducer {

    private final ByteArrayEventProducer eventProducer;

    private final KafkaTemplate<String, byte[]> kafkaTemplate;

    @Value("${declaration-service.kafka.declaration.topicname}")
    private String topicname;

    public void produce(DeclarationEventData declarationEventData) {
        eventProducer.produceEvent(
                kafkaTemplate,
                topicname,
                declarationEventData.getReference().toString(),
                declarationEventData.toByteArray());
    }

}
