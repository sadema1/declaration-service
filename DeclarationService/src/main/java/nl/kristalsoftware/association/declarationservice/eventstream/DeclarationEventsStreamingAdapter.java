package nl.kristalsoftware.association.declarationservice.eventstream;

import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.declarationservice.domain.declaration.streams.DeclarationEventsStreamingPort;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class DeclarationEventsStreamingAdapter implements DeclarationEventsStreamingPort {

    private final DeclarationEventDataProducer declarationEventDataProducer;

}
