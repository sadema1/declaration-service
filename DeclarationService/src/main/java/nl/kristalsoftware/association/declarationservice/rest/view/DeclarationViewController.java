package nl.kristalsoftware.association.declarationservice.rest.view;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.declarationservice.domain.declaration.views.DeclarationViewService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/declarations", produces = MediaType.APPLICATION_JSON_VALUE)
@Tag(name = "Declarations view", description = "Endpoints for retrieving declarations")
public class DeclarationViewController {

    private final DeclarationViewService declarationViewService;

}
