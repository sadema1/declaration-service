package nl.kristalsoftware.association.declarationservice.eventstore;

import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.declarationservice.domain.declaration.events.DeclarationEventsRepositoryFactoryPort;
import nl.kristalsoftware.association.declarationservice.domain.declaration.events.DeclarationEventsRepositoryPort;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class DeclarationEventsRepositoryFactoryAdapter implements DeclarationEventsRepositoryFactoryPort {

    private final DeclarationEventStoreRepository declarationEventStoreRepository;

    @Override
    public DeclarationEventsRepositoryPort createRepositoryAdapter() {
        return new DeclarationEventsRepositoryAdapter(declarationEventStoreRepository);
    }
}
