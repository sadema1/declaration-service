package nl.kristalsoftware.association.declarationservice.domain.declaration.streams;

public interface DeclarationEventsStreamingFactoryPort {
    DeclarationEventsStreamingPort createStreamingAdapter();
}
