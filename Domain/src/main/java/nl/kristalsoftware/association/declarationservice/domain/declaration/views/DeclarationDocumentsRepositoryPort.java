package nl.kristalsoftware.association.declarationservice.domain.declaration.views;

import nl.kristalsoftware.association.declarationservice.domain.declaration.aggregate.Declaration;
import nl.kristalsoftware.ddd.domain.base.view.DocumentsRepositoryPort;

public interface DeclarationDocumentsRepositoryPort extends DocumentsRepositoryPort<Declaration> {
}
