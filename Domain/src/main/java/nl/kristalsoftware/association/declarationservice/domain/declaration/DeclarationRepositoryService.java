package nl.kristalsoftware.association.declarationservice.domain.declaration;

import nl.kristalsoftware.association.declarationservice.domain.declaration.aggregate.Declaration;
import nl.kristalsoftware.association.declarationservice.domain.declaration.events.DeclarationEventsRepositoryFactoryPort;
import nl.kristalsoftware.association.declarationservice.domain.declaration.events.DeclarationEventsRepositoryPort;
import nl.kristalsoftware.association.declarationservice.domain.declaration.streams.DeclarationEventsStreamingFactoryPort;
import nl.kristalsoftware.association.declarationservice.domain.declaration.streams.DeclarationEventsStreamingPort;
import nl.kristalsoftware.association.declarationservice.domain.declaration.views.DeclarationDocumentsRepositoryFactoryPort;
import nl.kristalsoftware.association.declarationservice.domain.declaration.views.DeclarationDocumentsRepositoryPort;
import nl.kristalsoftware.ddd.domain.base.aggregate.AggregateRepositoryService;
import nl.kristalsoftware.ddd.domain.base.event.EventsRepositoryPort;
import nl.kristalsoftware.ddd.domain.base.view.DocumentsRepositoryPort;
import org.springframework.stereotype.Service;

@Service
public class DeclarationRepositoryService implements AggregateRepositoryService<Declaration> {
    private final DeclarationEventsRepositoryPort declarationEventsRepositoryPort;
    private final DeclarationDocumentsRepositoryPort declarationDocumentsRepositoryPort;
    private final DeclarationEventsStreamingPort declarationEventsStreamingPort;

    public DeclarationRepositoryService(DeclarationEventsRepositoryFactoryPort declarationEventsRepositoryFactoryPort,
                                        DeclarationDocumentsRepositoryFactoryPort declarationDocumentsRepositoryFactoryPort,
                                        DeclarationEventsStreamingFactoryPort declarationEventsStreamingFactoryPort) {
        declarationEventsRepositoryPort = declarationEventsRepositoryFactoryPort.createRepositoryAdapter();
        declarationDocumentsRepositoryPort = declarationDocumentsRepositoryFactoryPort.createRepositoryAdapter();
        declarationEventsStreamingPort = declarationEventsStreamingFactoryPort.createStreamingAdapter();
    }

    @Override
    public EventsRepositoryPort<Declaration> getEventsRepository() {
        return declarationEventsRepositoryPort;
    }

    @Override
    public DocumentsRepositoryPort<Declaration> getDocumentsRepository() {
        return declarationDocumentsRepositoryPort;
    }

}
