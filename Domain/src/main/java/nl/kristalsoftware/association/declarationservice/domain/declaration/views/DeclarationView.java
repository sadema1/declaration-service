package nl.kristalsoftware.association.declarationservice.domain.declaration.views;

import lombok.AllArgsConstructor;
import lombok.Data;
import nl.kristalsoftware.association.declarationservice.domain.declaration.attributes.DeclarationReference;

@Data
@AllArgsConstructor(staticName = "of")
public class DeclarationView {
    private DeclarationReference declarationReference;
}
