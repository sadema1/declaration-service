package nl.kristalsoftware.association.declarationservice.domain.declaration.views;

public interface DeclarationDocumentsRepositoryFactoryPort {
    DeclarationDocumentsRepositoryPort createRepositoryAdapter();
}
