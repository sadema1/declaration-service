package nl.kristalsoftware.association.declarationservice.domain.declaration.aggregate;

import lombok.Data;
import nl.kristalsoftware.ddd.domain.base.annotation.AggregateRoot;
import nl.kristalsoftware.ddd.domain.base.annotation.DomainEntity;

@AggregateRoot
@DomainEntity
@Data
public class DeclarationDomainEntity {

    public DeclarationDomainEntity() {
    }

}
