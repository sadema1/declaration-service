package nl.kristalsoftware.association.declarationservice.domain.declaration.aggregate;

import nl.kristalsoftware.association.declarationservice.domain.declaration.attributes.DeclarationReference;
import nl.kristalsoftware.ddd.domain.base.aggregate.AggregateFactory;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class DeclarationAggregateFactory implements AggregateFactory<Declaration, DeclarationReference> {
    public Declaration createAggregate() {
        return Declaration.of(DeclarationReference.of(UUID.randomUUID()), false);
    }

    public Declaration createAggregate(DeclarationReference declarationReference) {
        return Declaration.of(declarationReference, true);
    }
}
