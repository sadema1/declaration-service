package nl.kristalsoftware.association.declarationservice.domain.declaration;

import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.declarationservice.domain.declaration.aggregate.Declaration;
import nl.kristalsoftware.association.declarationservice.domain.declaration.aggregate.DeclarationAggregateFactory;
import nl.kristalsoftware.association.declarationservice.domain.declaration.attributes.DeclarationReference;
import nl.kristalsoftware.ddd.domain.base.aggregate.AggregateRepository;

@Slf4j
public class DeclarationRepository extends AggregateRepository<DeclarationRepository, Declaration, DeclarationReference> {

    private final DeclarationRepositoryService declarationRepositoryService;

    private DeclarationRepository(DeclarationAggregateFactory declarationAggregateFactory,
                                  DeclarationRepositoryService declarationRepositoryService) {
        super(declarationAggregateFactory, declarationRepositoryService);
        this.declarationRepositoryService = declarationRepositoryService;
    }

    private DeclarationRepository(DeclarationReference declarationReference,
                                  DeclarationAggregateFactory declarationAggregateFactory,
                                  DeclarationRepositoryService declarationRepositoryService) {
        super(declarationReference, declarationAggregateFactory, declarationRepositoryService);
        this.declarationRepositoryService = declarationRepositoryService;
    }

    public static DeclarationRepository of(DeclarationAggregateFactory declarationAggregateFactory,
                                           DeclarationRepositoryService declarationRepositoryService) {
        return new DeclarationRepository(declarationAggregateFactory, declarationRepositoryService);
    }

    public static DeclarationRepository of(DeclarationReference declarationReference,
                                           DeclarationAggregateFactory declarationAggregateFactory,
                                           DeclarationRepositoryService declarationRepositoryService) {
        return new DeclarationRepository(declarationReference, declarationAggregateFactory, declarationRepositoryService);
    }

    @Override
    protected DeclarationRepository getDomainRepository() {
        return this;
    }

}
