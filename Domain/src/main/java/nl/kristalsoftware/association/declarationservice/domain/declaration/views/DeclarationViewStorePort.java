package nl.kristalsoftware.association.declarationservice.domain.declaration.views;

import nl.kristalsoftware.association.declarationservice.domain.declaration.attributes.DeclarationReference;
import nl.kristalsoftware.ddd.domain.base.aggregate.AggregateNotFoundException;

import java.util.List;

public interface DeclarationViewStorePort {
    List<DeclarationView> getAllDeclarations();

    DeclarationView getDeclarationByReference(DeclarationReference declarationReference) throws AggregateNotFoundException;

}
