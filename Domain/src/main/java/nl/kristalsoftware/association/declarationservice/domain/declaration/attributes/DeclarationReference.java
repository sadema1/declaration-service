package nl.kristalsoftware.association.declarationservice.domain.declaration.attributes;


import nl.kristalsoftware.ddd.domain.base.annotation.ValueObject;
import nl.kristalsoftware.ddd.domain.base.type.TinyUUIDType;

import java.util.UUID;

@ValueObject
public class DeclarationReference extends TinyUUIDType {

    private DeclarationReference(UUID value) {
        super(value);
    }

    public static DeclarationReference of(UUID value) {
        return new DeclarationReference(value);
    }

    public static DeclarationReference of(String value) {
        UUID uuid = null;
        if (value != null && !value.isEmpty() && !value.equals("0")) {
            uuid = UUID.fromString(value);
        }
        return new DeclarationReference(uuid);
    }

}
