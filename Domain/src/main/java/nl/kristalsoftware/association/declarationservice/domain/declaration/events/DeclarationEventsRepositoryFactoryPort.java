package nl.kristalsoftware.association.declarationservice.domain.declaration.events;

public interface DeclarationEventsRepositoryFactoryPort {
    DeclarationEventsRepositoryPort createRepositoryAdapter();
}
