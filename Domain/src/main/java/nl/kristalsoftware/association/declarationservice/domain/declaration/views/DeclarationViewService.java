package nl.kristalsoftware.association.declarationservice.domain.declaration.views;

import lombok.RequiredArgsConstructor;
import nl.kristalsoftware.association.declarationservice.domain.declaration.attributes.DeclarationReference;
import nl.kristalsoftware.ddd.domain.base.aggregate.AggregateNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class DeclarationViewService {

    private final DeclarationViewStorePort declarationViewStorePort;

    public List<DeclarationView> getAllDeclarations() {
        return declarationViewStorePort.getAllDeclarations();
    }

    public DeclarationView getDeclarationByReference(DeclarationReference declarationReference) throws AggregateNotFoundException {
        return declarationViewStorePort.getDeclarationByReference(declarationReference);
    }
}
