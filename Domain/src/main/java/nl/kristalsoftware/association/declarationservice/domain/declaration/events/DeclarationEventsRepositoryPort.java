package nl.kristalsoftware.association.declarationservice.domain.declaration.events;

import nl.kristalsoftware.association.declarationservice.domain.declaration.aggregate.Declaration;
import nl.kristalsoftware.ddd.domain.base.event.EventsRepositoryPort;

public interface DeclarationEventsRepositoryPort extends EventsRepositoryPort<Declaration> {
}
