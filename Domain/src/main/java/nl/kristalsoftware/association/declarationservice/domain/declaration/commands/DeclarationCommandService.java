package nl.kristalsoftware.association.declarationservice.domain.declaration.commands;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.declarationservice.domain.declaration.DeclarationRepository;
import nl.kristalsoftware.association.declarationservice.domain.declaration.DeclarationRepositoryService;
import nl.kristalsoftware.association.declarationservice.domain.declaration.aggregate.Declaration;
import nl.kristalsoftware.association.declarationservice.domain.declaration.aggregate.DeclarationAggregateFactory;
import nl.kristalsoftware.ddd.domain.base.annotation.DomainService;
import nl.kristalsoftware.ddd.domain.base.command.BaseCommandService;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
@DomainService
public class DeclarationCommandService extends BaseCommandService<DeclarationRepository, Declaration> {

    private final DeclarationAggregateFactory declarationAggregateFactory;
    private final DeclarationRepositoryService declarationRepositoryService;

}
