package nl.kristalsoftware.association.declarationservice.domain.declaration.aggregate;

import lombok.extern.slf4j.Slf4j;
import nl.kristalsoftware.association.declarationservice.domain.declaration.attributes.DeclarationReference;
import nl.kristalsoftware.ddd.domain.base.aggregate.BaseAggregateRoot;

@Slf4j
public class Declaration extends BaseAggregateRoot<DeclarationReference> {

    private DeclarationDomainEntity declarationDomainEntity = new DeclarationDomainEntity();

    private Declaration(DeclarationReference declarationReference, Boolean existingAggregate) {
        super(declarationReference, existingAggregate);
    }

    private Declaration(DeclarationReference declarationReference, Long version) {
        super(declarationReference, version);
    }

    public static Declaration of(DeclarationReference declarationReference, Boolean existingAggregate) {
        return new Declaration(declarationReference, existingAggregate);
    }

    public static Declaration of(DeclarationReference declarationReference, Long version) {
        return new Declaration(declarationReference, version);
    }

}
