### README_steps.md

## Value Objects

```java
@ValueObject
public class MemberName extends TinyStringType {

    @Getter
    final private String firstName;
    @Getter
    final private String lastName;

    private MemberName(String firstName, String lastName) {
        super(firstName + " " + lastName);
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public static MemberName of(String firstName, String lastName) {
        return new MemberName(firstName, lastName);
    }

}
```

## Commands

```java
@RequiredArgsConstructor(staticName = "of")
public class SignUpMember implements BaseCommand<MemberRepository, Member> {

    @Getter
    private final MemberName memberName;
    @Getter
    private final MemberBirthDate memberBirthDate;
    @Getter
    private final MemberKind memberKind;

    @Override
    public List<DomainEventSaving<MemberRepository>> handleCommand(Member member) {
        return member.handleCommand(this);
    }
}
```

## CommandService

```
    public Optional<MemberReference> signUpMember(
            MemberName memberName,
            MemberBirthDate memberBirthDate,
            MemberKind memberKind) {
        MemberRepository memberRepository = MemberRepository.of(memberAggregateFactory, memberRepositoryService);
        List<DomainEventSaving<MemberRepository>> domainEventList = sendCommand(SignUpMember.of(memberName, memberBirthDate, memberKind), memberRepository.getAggregate());
        if (memberRepository.saveEvents(domainEventList)) {
            return Optional.of(memberRepository.getAggregate().getReference());
        }
        return Optional.empty();
    }
```

## Events

```java
@DomainEvent
@Getter
@RequiredArgsConstructor(staticName = "of")
public class MemberSignedUp implements DomainEventLoading<Member>, DomainEventSaving<MemberRepository> {

    private final MemberReference memberReference;

    private final MemberName memberName;

    private final MemberBirthDate memberBirthDate;

    private final MemberKind memberKind;

    @Override
    public void load(Member aggregate) {
        aggregate.loadEventData(this);
    }

    @Override
    public void save(MemberRepository memberRepository) {
        memberRepository.save(this);
    }

}
```

## Event entities

```java
@NoArgsConstructor
@Data
@Entity(name = "MemberSignedUpEvent")
public class MemberSignedUpEventEntity extends DeclarationBaseEventEntity<MemberSignedUp> {

    private String firstName;

    private String lastName;

    private LocalDate birthDate;

    @Enumerated(EnumType.STRING)
    private Kind kind;

    private MemberSignedUpEventEntity(
            UUID reference,
            String domainEventName,
            String firstName,
            String lastName,
            LocalDate birthDate,
            Kind kind
    ) {
        super(reference, domainEventName);
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.kind = kind;
    }

    public static MemberSignedUpEventEntity of(MemberSignedUp memberSignedUp) {
        return new MemberSignedUpEventEntity(
                memberSignedUp.getDeclarationReference().getValue(),
                memberSignedUp.getClass().getSimpleName(),
                memberSignedUp.getMemberName().getFirstName(),
                memberSignedUp.getMemberName().getLastName(),
                TinyDateType.getLocalDateFromMillis(memberSignedUp.getMemberBirthDate().getDateInMillis()),
                memberSignedUp.getMemberKind().getValue()
        );
    }


    @Override
    public MemberSignedUp getDomainEvent() {
        return MemberSignedUp.of(
                DeclarationReference.of(getReference()),
                MemberName.of(firstName, lastName),
                MemberBirthDate.of(birthDate),
                MemberKind.of(kind)
        );
    }

}
```

## EventAggregate

```java
@Slf4j
public class Member extends BaseAggregateRoot<MemberReference> {

    private MemberDomainEntity memberDomainEntity = new MemberDomainEntity();

    private Member(MemberReference memberReference, Boolean existingAggregate) {
        super(memberReference, existingAggregate);
    }

    private Member(MemberReference memberReference, Long version) {
        super(memberReference, version);
    }

    public static Member of(MemberReference memberReference, Boolean existingAggregate) {
        return new Member(memberReference, existingAggregate);
    }

    public static Member of(MemberReference memberReference, Long version) {
        return new Member(memberReference, version);
    }

    public List<DomainEventSaving<MemberRepository>> handleCommand(SignUpMember signUpMember) {
        List<DomainEventSaving<MemberRepository>> domainEventList = new ArrayList<>();
        domainEventList.add(MemberSignedUp.of(
                getReference(),
                signUpMember.getMemberName(),
                signUpMember.getMemberBirthDate(),
                signUpMember.getMemberKind()
                )
        );
        return domainEventList;
    }

    public List<DomainEventSaving<MemberRepository>> handleCommand(ChangeMemberKind changeMemberKind) {
        List<DomainEventSaving<MemberRepository>> domainEventList = new ArrayList<>();
        domainEventList.add(MemberKindChanged.of(getReference(), changeMemberKind.getMemberKind()));
        return domainEventList;
    }

    public void loadEventData(MemberSignedUp memberSignedUp) {
        memberDomainEntity.setMemberName(memberSignedUp.getMemberName());
        memberDomainEntity.setMemberBirthDate(memberSignedUp.getMemberBirthDate());
        memberDomainEntity.setMemberKind(memberSignedUp.getMemberKind());
    }

    public void loadEventData(MemberKindChanged memberKindChanged) {
        memberDomainEntity.setMemberKind(memberKindChanged.getMemberKind());
    }

}
```

## EventsRepositoryAdapter

```java
@RequiredArgsConstructor
public class MemberEventsRepositoryAdapter implements MemberEventsRepositoryPort {

    private final MemberEventStoreRepository memberEventStoreRepository;

    @Override
    public void saveEvent(MemberSignedUp memberSignedUp) {
        memberEventStoreRepository.save(MemberSignedUpEventEntity.of(memberSignedUp));
    }

    @Override
    public List<DomainEventLoading<Member>> findAllDomainEventsByReference(Member aggregate) {
        List<DomainEventLoading<Member>> list = new ArrayList<>();
        for (MemberBaseEventEntity<? extends DomainEventLoading<Member>> it : memberEventStoreRepository.findAllByReference(aggregate.getReference().getValue())) {
            DomainEventLoading<Member> domainEvent = it.getDomainEvent();
            list.add(domainEvent);
        }
        return list;
    }
}
```

## DocumentsRepositoryAdapter

```java
@Slf4j
@RequiredArgsConstructor
public class MemberDocumentsRepositoryAdapter implements MemberDocumentsRepositoryPort {

    private final MemberDocumentsRepository memberDocumentsRepository;
    private MemberDocument memberDocument = new MemberDocument();

    @Override
    public void createDocument(Member aggregate) {
        Optional<MemberDocument> result = memberDocumentsRepository.findByReference(aggregate.getReference().getValue());
        memberDocument = result.orElseThrow(() -> new IllegalStateException(String.format("Document with reference %s not found in viewstore!", aggregate.getReference().getValue())));
    }

    @Override
    public void saveDocument(MemberSignedUp memberSignedUp) {
        memberDocument = MemberDocument.of(
                memberSignedUp.getMemberReference().getValue(),
                memberSignedUp.getMemberName().getFirstName(),
                memberSignedUp.getMemberName().getLastName(),
                memberSignedUp.getMemberBirthDate().getDateInMillis(),
                memberSignedUp.getMemberKind().getValue().name()
        );
    }

    @Override
    public void saveDocument(MemberKindChanged memberKindChanged) {
        memberDocument.setKind(memberKindChanged.getMemberKind().getValue().name());
    }

    @Override
    public void saveDocument() {
        memberDocumentsRepository.save(memberDocument);
    }

}
```
